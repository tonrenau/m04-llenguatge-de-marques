UF1 Programació amb XML
Activitat 6

1. XSD

Realitza l'esquema (XML Schema) adient que validi el següent document XML:

<agenda any="2013" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="agenda.xsd">
    <propietari>
        <nom>Eliot Ferrer Smith</nom>
        <email>eliot.ferrer1234@gmail.com</email>
    </propietari>
    <contactes>
        <contacte>
            <nom>Peter Brown</nom>
            <telefon>633124751</telefon>
            <telefon>933334565</telefon>
            <email>peter.b@gmail.com</email>
        </contacte>
        <contacte>
            <nom>Anne Williams</nom>
            <telefon>934152234</telefon>
            <email>george-Will@mailing.co.uk</email>
        </contacte>
        <contacte>
            <nom>Gisela Ventura</nom>
            <telefon>934152234</telefon>
            <email>GisVent123@xtec.cat</email>
        </contacte>
    </contactes>
</agenda>


S'ha de tenir en compte que:

El propietari només ha de tenir un atribut nom i un atribut email de tipus string.
Contactes pot tenir 1 contacte o molts.
Un contacte pot tenir 1 o 2 telèfons i ha de ser una nombre de 9 xifres, entre "00000001" i "999999999".
Un contacte ha de tenir només 1 email i ha de tenir un format correcte.

Nota:
Utilitza aquest pattern per l'email:

([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})



Reposta:
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="agenda">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="propietari"> 
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="propietari" type="xs:string"></xs:element>
                            <xs:element name="email" maxOccurs="1">
                                <xs:simpleType> 
                                    <xs:restriction base="xs:string">
                                        <xs:pattern value="([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})"></xs:pattern>
                                    </xs:restriction>
                                </xs:simpleType>
                            </xs:element>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
                <xs:element name="conatacte" maxOccurs="unbounded">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="nom" type="xs:string"></xs:element>
                            <xs:element name="telefon" maxOccurs="2">
                                <xs:simpleType>
                                    <xs:restriction base="xs:integer">
                                        <xs:pattern value="[0-9]{9}"></xs:pattern>
                                    </xs:restriction>
                                </xs:simpleType>
                            </xs:element>
                            <xs:element name="email" maxOccurs="1">
                                <xs:simpleType>
                                    <xs:restriction base="xs:string">
                                        <xs:pattern value="([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})"></xs:pattern>
                                    </xs:restriction>
                                </xs:simpleType>
                            </xs:element>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
            </xs:sequence>
            <xs:attribute name="any" type="xs:integer"></xs:attribute>
        </xs:complexType>
    </xs:element>
</xs:schema>


2. XSD

Realitza el document XML i l'esquema (XML Schema) adient que correspongui a l'arbre que es mostra a continuació, tenint en compte les condicions que s'indiquen.



Condicions:

1. El número de societat (N_societat) constarà de tres xifres numèriques seguides d'una lletra majúscula, intentant minimitzar al màxim l'expressió (utilitza un pattern).

2. El nom de la societat tindrà com a màxim 15 caràcters i només contindrà lletres i números majúscules o minúscules i espais (lletres accentuades també les ha d'acceptar).

3. L'any de fundació estarà entre 1500 i 2050 ambdós inclosos.

4. En quant al president i soci, el nom tindrà entre 1 i 20 caràcters i els cognoms tindran entre 1 i 30 caràcters.

5. El nif constarà de 8 dígits numèrics més un digit de la “a” a la “z” que pot ser majúscula o minúscula sense signes de puntuació ni guions.

6. Com que es tracta d'una societat limitada, tan sols són admissibles de 0 a 150 socis.




Reposta:


