UF1 Programació amb XML
Activitat 5

1. XSD

Realitza l'esquema (XML Schema) adient que validi el següent document XML:

<?xml version="1.0" encoding="UTF-8"?>
<exercici1 xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='exercici1.xsd'>
    <ciudad>Roma</ciudad>
    <fecha-de-nacimiento>1996-12-18</fecha-de-nacimiento>
    <hora>18:29:45</hora>
    <nota>7.5</nota>
    <apto>true</apto>
</exercici1>



Reposta:

<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="exercici1">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="ciudad" type="xs:string" />
                <xs:element name="fecha-de-nacimiento" type="xs:date" />
                <xs:element name="hora" type="xs:time"/>
                <xs:element name="nota" type="xs:decimal"/>
                <xs:element name="apto" type="xs:boolean"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
</xs:schema>

2. XSD

Definir un element anomenat portatancada de tipus lògic, que per defecte tingui el valor "fals", i un altre element anomenat finestraoberta també de tipus lògic, que tingui assignat el valor fix "veritable".


<?xml version="1.0" encoding="UTF-8"?>
<exercici2 xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='exercici2.xsd'>
    <portatancada>false</portatancada>
    <portaoberta>true</portaoberta>
</exercici2>


Reposta:

<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="exercici2">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="portatancada" type="xs:boolean" default="false"/>
                <xs:element name="finestraoberta" type="xs:string" fixed="veritable"/> 
            </xs:sequence>
        </xs:complexType>
    </xs:element>   
</xs:schema>




3. XSD

Donat el següent document XML, escriure el contingut de l'arxiu "fitxes.xsd" que permeti validar-lo.

<?xml version="1.0" encoding="UTF-8"?>
<fitxes xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="fitxes.xsd">
    <fitxa numero="1">
        <nom>Ana Sanz Tin</nom>
        <edat>22</edat>
    </fitxa>
    <fitxa numero="2">
        <nom>Iker Rubio Mol</nom>
        <edat>23</edat>
    </fitxa>
</fitxes>

Resposta:

<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="fitxes.xml">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="fitxa" maxOccurs="unbounded">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="nom" type="xs:string"/>
                            <xs:element name="edat" type="xs:integer"/>
                        </xs:sequence>
                        <xs:attribute name="numero">
                            <xs:simpleType>
                                <xs:restriction base="xs:integer">
                                    <xs:maxInclusive value="10"></xs:maxInclusive>
                                </xs:restriction>
                            </xs:simpleType>
                        </xs:attribute>
                    </xs:complexType>    
                </xs:element>
            </xs:sequence>           
        </xs:complexType>
    </xs:element>
</xs:schema>


4. XSD

Donat el següent document XSD, escriure el contingut de l'arxiu "fitxes2.xml" que permeti validar-lo.


<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="bingo">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="bola" maxOccurs="unbounded">
                    <xs:complexType>
                        <xs:attribute name="numero" type="numeroDeBola"/>
                    </xs:complexType>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:simpleType name="numeroDeBola">
        <xs:restriction base="xs:positiveInteger">
            <xs:minInclusive value="1"/>
            <xs:maxExclusive value="90"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>



Reposta:

<?xml version="1.0" encoding="UTF-8"?>
<bingo xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="fitxes2.xsd">
    <bola numero="1"></bola>
    <bola numero="63"></bola>
    <bola numero="89"></bola>
</bingo>


5. XSD

Donat l’arxiu "persones.xsd". Escriure el codi d'un document XML que pugui ser validat pel fitxer XSD i que emmagatzemi la següent informació:

"Eva viu a París i té 25 anys."
"Giovanni viu a Florència i té 26 anys."


<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="persones">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="persona" maxOccurs="unbounded">
                    <xs:complexType mixed="true">
                        <xs:sequence>
                            <xs:element name="nom" type="xs:string"/>
                            <xs:element name="ciutat" type="xs:string"/>
                            <xs:element name="edat" type="xs:positiveInteger"/>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
</xs:schema>


Escriure el contingut de l'arxiu "persones.xml" que permeti validar-lo.


Reposta:

<?xml version="1.0" encoding="UTF-8"?>
<persones xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='persones.xsd'>
   <persona>
     <nom>Eva</nom> viu a <ciutat>Paris</ciutat> i té <edat>25</edat> anys
   </persona>
   <persona>
     <nom>Giovanni</nom> viu a <ciutat>Florència</ciutat> i té <edat>26</edat> anys
   </persona>
</persones>
