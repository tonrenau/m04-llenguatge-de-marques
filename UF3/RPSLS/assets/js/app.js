let UserPoints = 0;
let PcPoints = 0;

function turn(move) {
    var imagen = move.target.src;
    var userChoiceDisplay = document.querySelector('#table_play tr:nth-child(2) td:nth-child(1)');
    userChoiceDisplay.innerHTML = '<img src="' + imagen + '" alt="foto" class="option">';
}

function play() {
    var computerChoiceDisplay = document.querySelector('#table_play tr:nth-child(2) td:nth-child(2)');
    computerChoiceDisplay.innerHTML = '';

    var computerChoices = ['rock', 'paper', 'scissors', 'lizard', 'spock'];
    var timeoutDuration = 50;
    var maxTimeouts = 15;

    function rotar() {
        var randomIndex = Math.floor(Math.random() * computerChoices.length);
        var computerChoice = computerChoices[randomIndex];
        computerChoiceDisplay.innerHTML = '<img src="./assets/images/' + computerChoice + '.png" alt="foto" class="option">';
        maxTimeouts--;
        if (maxTimeouts > 0) {
            setTimeout(rotar, timeoutDuration);
        } else {
            winner(computerChoice);
        }
    }

    function winner(computerChoice) {
        var userChoiceDisplay = document.querySelector('#table_play tr:nth-child(3) td:nth-child(1)');
        var computerChoiceDisplay = document.querySelector('#table_play tr:nth-child(3) td:nth-child(2)');

        userChoiceDisplay.innerHTML = 'Points: ' + userPoints;
        computerChoiceDisplay.innerHTML = 'Points: ' + computerPoints;

        var userChoice = document.querySelector('#table_play tr:nth-child(2) td:nth-child(1) img').src;

        userChoice = userChoice.substring(userChoice.lastIndexOf('/') + 1, userChoice.lastIndexOf('.png'));

        var resultDisplay = document.getElementById("result");

        if (userChoice === computerChoice) {
            resultDisplay.innerText = "Result: DRAW!";        
        } else if (
            (userChoice == "rock" && (computerChoice == "scissors" || computerChoice == "lizard")) ||
            (userChoice == "paper" && (computerChoice == "rock" || computerChoice == "spock")) ||
            (userChoice == "scissors" && (computerChoice == "paper" || computerChoice == "lizard")) ||
            (userChoice == "lizard" && (computerChoice == "spock" || computerChoice == "paper")) ||
            (userChoice == "spock" && (computerChoice == "rock" || computerChoice == "scissors"))
        ) {
            resultDisplay.innerText = "Result: PLAYER WIN!";            
            userPoints++;
        } else {
            resultDisplay.innerText = "Result: PC WIN!";
            computerPoints++;
        }

        document.getElementById("UserPoints").innerText = "Points: " + userPoints;
        document.getElementById("PcPoints").innerText = "Points: " + computerPoints;
    }

    setTimeout(rotar, timeoutDuration);
}






