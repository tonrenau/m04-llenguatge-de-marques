<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head> 
            <title>Bookstore</title>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" href="bookstore.css"/>
        </head>
        <body>
            <h4>Bookstore</h4>
            <table id="table">
            <xsl:for-each select="/bookstore/book">
                <tr>
                    <th>Title (lang:<xsl:value-of select="title/@lang"/>)</th>
                    <td><xsl:value-of select="title"/> </td>
                </tr>
                <tr>
                    <th>Category </th>
                    <td><xsl:value-of select="@category"/> </td>
                </tr>
                <tr>
                    <th>Year</th>
                    <td><xsl:value-of select="year"/> </td>
                </tr>
                <tr>
                    <th>price </th>
                    <td><xsl:value-of select="price"/> </td>
                </tr>
                <tr>
                    <th>Format </th>
                    <td><xsl:value-of select="format/@type"/> </td>
                </tr>
                <tr>
                    <th>ISBN </th>
                    <td><xsl:value-of select="isbn"/> </td>
                </tr>
                <tr>
                    <th id="authors">Authors </th>
                </tr>
                <tr>
                    <td><xsl:value-of select="author"/> </td>
                </tr>
            </xsl:for-each>
            </table>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>
