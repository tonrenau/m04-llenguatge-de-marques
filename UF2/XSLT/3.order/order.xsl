﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head> 
            <title>Order</title>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" href="order.css"/>
        </head>
        <body>
            <h4>Products with a Price > 25 and Price &lt;= 100 </h4>
            <custom>
                <div class="custom">
                    <table>
                        <thead>
                            <tr>
                                <td colspan="2">CUSTOMER DETAILS</td>
                            </tr>
                        </thead>
                        <tbody>
                        <xsl:for-each select="//destination">
                            <tr>
                                <th>Name</th>
                                <td><xsl:value-of select="name"/> </td>
                            </tr>
                            <tr>
                                <th>Address </th>
                                <td><xsl:value-of select="address"/> </td>
                            </tr>
                            <tr>
                                <th>City </th>
                                <td><xsl:value-of select="city"/> </td>
                            </tr>
                            <tr>
                                <th>P.C. </th>
                                <td><xsl:value-of select="postalcode"/> </td>
                            </tr>
                        </xsl:for-each>
                        </tbody>
                    </table>
                </div>
                <div class="order">
                    <table>
                        <thead>
                            <tr>
                                <td colspan="4">ORDER</td>
                            </tr>
                            <tr>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        <xsl:for-each select="/order/products/product[price > 25 and price <= 100]">
                            <tr>
                                <xsl:choose>
                                    <xsl:when test="price > 25 and price < 50">
                                        <xsl:attribute name="style">background-color: yellow;</xsl:attribute>
                                    </xsl:when>
                                    <xsl:when test="price >= 50 and price < 75">
                                        <xsl:attribute name="style">background-color: green;</xsl:attribute>
                                    </xsl:when>
                                    <xsl:when test="price >= 75 and price <= 100">
                                        <xsl:attribute name="style">background-color: red;</xsl:attribute>
                                    </xsl:when>
                                </xsl:choose>
                                <th>
                                <xsl:value-of select="name"/>
                                (Code:<xsl:value-of select="@code"/>)
                                </th>
                                <th><xsl:value-of select="price"/></th>
                                <th><xsl:value-of select="quantity"/></th>
                                <th>
                                    <xsl:variable name="price" select="price"/>
                                    <xsl:variable name="quantity" select="quantity"/>
                                    <xsl:value-of select="$price * $quantity"/>
                                </th>
                            </tr>
                        </xsl:for-each>
                        </tbody>
                    </table>
                </div>
            </custom>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>

