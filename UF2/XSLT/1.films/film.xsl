<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head> 
            <title>FILM LIST</title>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" href="film.css"/>
        </head>
        <body>
            <h1>FILM LIST</h1>
            <table border="1">
                <tr>
                    <th>Title</th>
                    <th>Language</th>
                    <th>year</th>
                    <th>country</th>
                    <th>genere</th>
                    <th>sumary</th>
                </tr>
                <xsl:for-each select="//film">
                    <tr>
                        <td>
                            <xsl:value-of select="title"/>
                        </td>
                        <td>
                            <xsl:value-of select="title/@lang"/>
                        </td>
                        <td>
                            <xsl:value-of select="year"/>
                        </td>
                        <td>
                            <xsl:value-of select="country"/>
                        </td>
                        <td>
                            <xsl:value-of select="genre"/>
                        </td>
                        <td>
                            <xsl:value-of select="summary"/>
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>