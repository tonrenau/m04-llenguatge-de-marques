<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="pokedex.css"/>
    <title>pokedex</title>
</head>
<body>
    <h1>Pokemons</h1>
    <table>
        <tr class="head"> 
            <th>Image </th>
            <th>Pokemon </th>
            <th>HP </th>
            <th>ATK </th>
            <th>DEF </th>
            <th>SPD </th>
            <th>SATK </th>
            <th>SDEF </th>
        </tr>
        {
            for $x in doc("./pokedex.xml")/pokedex/pokemon
            let $image := $x/image
            return 
        
            <tr>
                <td>
                {
                    <img src="{data($image)}" width="20%"></img>

                }
                </td>
                <td> 
                {
                    data($x/species)
                }
                </td>
                <td>
                {
                    data($x/baseStats/HP)
                }
                </td>
                <td>
                {
                    data($x/baseStats/ATK)
                }
                </td>
                <td>
                {
                    data($x/baseStats/DEF)
                }
                </td>
                <td>
                {
                    data($x/baseStats/SPD)
                }
                </td>
                <td>
                {
                    data($x/baseStats/SATK)
                }
                </td>
                <td>
                {
                    data($x/baseStats/SDEF)
                }
                </td>
            </tr>
        }
    </table>
</body>
</html>