for $x in doc("./pokedex.xml")/pokedex/pokemon
order by $x/species
return 
<pokemons>
{
  <pokemon>
    {
      ($x/species, $x/abilities)
    }
  </pokemon>
}
</pokemons>