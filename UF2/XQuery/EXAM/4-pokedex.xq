for $x in doc("./pokedex.xml")/pokedex/pokemon
where $x/types/type = "FLYING"
order by $x/species
return 
<pokemons>
{
  <pokemon>
    {
      ($x/species, $x/types)
    }
  </pokemon>
}
</pokemons>