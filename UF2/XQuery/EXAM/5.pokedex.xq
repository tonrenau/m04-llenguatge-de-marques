for $x in doc("./pokedex.xml")/pokedex/pokemon
where $x/baseStats/ATK > 100 
order by $x/baseStats/ATK 
return 
<pokemons>
{
  <pokemon>
    {
      ($x/species, $x/baseStats/ATK)
    }
  </pokemon>
}
</pokemons>