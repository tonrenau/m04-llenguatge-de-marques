for $x in doc("./pokedex.xml")/pokedex/pokemon
let $total := sum($x/baseStats/HP + $x/baseStats/ATK + $x/baseStats/DEF + $x/baseStats/SPD + $x/baseStats/SATK + $x/baseStats/SDEF)
order by $total descending
return 
<pokemons>
{
  <pokemon>
    {
      ($x/species,
       <total>{$total}</total>
      )
    }
  </pokemon>
}
</pokemons>