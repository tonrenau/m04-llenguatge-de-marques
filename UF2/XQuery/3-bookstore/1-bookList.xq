<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Bookstore</title>
    <link rel="stylesheet" href="bookList.css"/>
</head>
<body>
    <table>
        <tr>
            <th>Title</th>
            <th>Editorial</th>
            <th>Price</th>
        </tr>
        {
            for $x in doc('./bookstore.xml') /bookstore/book
            return 
            <tr>
                <td>{data($x/title)}</td>
                <td>{$x/editorial}</td>
                <td>{$x/price}</td>
            </tr>
        }
    </table>
</body>
</html>