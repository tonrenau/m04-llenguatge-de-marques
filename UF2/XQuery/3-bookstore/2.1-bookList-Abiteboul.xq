
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Bookstore</title>
    <link rel="stylesheet" href="bookList.css"/>
</head>
<body>
    <table>
        <tr>
            <th colspan="5" class="title">
            {
                let $lastname := "Abiteboul"
                return 
                <p>{ data($lastname)} books</p>
            }
            </th>
        </tr>
        <tr>
            <th>Title</th>
            <th>year</th>
            <th>Author</th>
            <th>Editorial</th>
            <th>price</th>
        </tr>
        {
            let $lastname := "Abiteboul"
            for $x in doc('./bookstore.xml') /bookstore/book[author/lastname= $lastname] 
            let $year := $x/@year
            return 
            <tr>
                <td>
                {
                    data($x/title)
                }
                </td>
                <td>
                {
                    data($year)
                }
                </td>
                <td>
                {
                    <p>{data(($x/author/name, $x/author/lastname))}</p>
                }
                </td>
                <td>
                {
                    data($x/editorial)
                }
                </td>
                <td>
                {
                    data($x/price)
                }
                </td>
            </tr>
        }
    </table>
            {
                let $lastname := "Abiteboul"
                let $price := sum(doc('./bookstore.xml')/bookstore/book[author/lastname = $lastname]/price)
                return 
                <p>Total price: {sum($price)}</p>
            }
</body>
</html>