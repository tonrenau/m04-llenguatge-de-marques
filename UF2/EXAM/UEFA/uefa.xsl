<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head> 
            <title>UEFA</title>
            <meta charset="UTF-8"/>
            <link rel="stylesheet" href="uefa.css"/>
        </head>
        <body>
            <xsl:for-each select="UEFA/season/organization">
                <xsl:variable name="uefa" select="logo_uefa"/>
                <p><img src="{$uefa}" style="height: 75px; width: 75px"/></p>
                <h2>UEFA Champions Leage</h2>
            </xsl:for-each>
                <table>
                    <thead>
                        <tr>
                            <td colspan="7"> Quarter finals </td>
                            <td> Winner</td>
                        </tr>
                    </thead>
                    <xsl:for-each select="UEFA/season/quarter-finals/match">
                        <tbody>
                            <tr>
                                <td>
                                    <xsl:variable name="team" select="first-leg/local/logo"/>
                                    <img src="{$team}" style="height: 50px; width: 50px"/>
                                </td>
                                <td><xsl:value-of select="first-leg/local/name"/></td>
                                <td><xsl:value-of select="first-leg/local/goals"/></td>
                                <td>-</td>
                                <td><xsl:value-of select="first-leg/visitant/goals"/></td>
                                <td><xsl:value-of select="first-leg/visitant/name"/></td>
                                <td>
                                    <xsl:variable name="team" select="first-leg/visitant/logo"/>
                                    <img src="{$team}" style="height: 50px; width: 50px"/>
                                </td>
                                <td rowspan="2">
                                    <xsl:variable name="win" select="//winner"/>
                                    <xsl:choose>
                                        <xsl:when test="first-leg/local/id = $win">
                                            <xsl:variable name="logo_win" select="first-leg/local/logo"/>
                                            <img src="{$logo_win}" style="height: 50px; width: 50px"/>
                                        </xsl:when>
                                        <xsl:when test="first-leg/visitant/id = $win">
                                            <xsl:variable name="logo_win" select="first-leg/visitant/logo"/>
                                            <img src="{$logo_win}" style="height: 50px; width: 50px"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </td> 
                            </tr>
                            <tr>
                                <td>
                                    <xsl:variable name="team" select="second-leg/local/logo"/>
                                    <img src="{$team}" style="height: 50px; width: 50px"/>
                                </td>
                                <td><xsl:value-of select="second-leg/local/name"/></td>
                                <td><xsl:value-of select="second-leg/local/goals"/></td>
                                <td>-</td>
                                <td><xsl:value-of select="second-leg/visitant/goals"/></td>
                                <td><xsl:value-of select="second-leg/visitant/name"/></td>
                                <td>
                                    <xsl:variable name="team" select="second-leg/visitant/logo"/>
                                    <img src="{$team}" style="height: 50px; width: 50px"/>
                                </td>  
                                
                            </tr>
                        </tbody>
                    </xsl:for-each>
                </table>
                <p>Total goals: <xsl:value-of select="sum(//goals)"/></p>
                <p>Number of matches: <xsl:value-of select="count(//match)*2"/></p>
                <p>Average goals per match: <xsl:value-of select="round(sum(//goals) div (count(//match)*2))"/></p>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>