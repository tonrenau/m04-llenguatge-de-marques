# UF2 Àmbits d’aplicació de l’XML 
## NF1 RSS

## Activitat 1. Generar un fitxer RSS 

Utilitza una eina online per generar el fitxer rss d'una pàgina web que tingui articles o notícies (la que més t'agradi).

### 1. Copia la informació de l'etiqueta <channel> (sense considerar l'etiqueta <item>) i fixa't quines etiquetes ha afegit.
    <channel>
		<title>Homepage - World Skate Europe</title>
		<link><![CDATA[https://europe.worldskate.org]]></link>
		<description>The official schedule of WSE Champions League Women Final Four | Lisbon 2023...</description>
		<language>en</language>
		<pubDate>Fri, 24 Nov 2023 09:24:12 +0000</pubDate>
		<generator>MySitemapGenerator (www.mysitemapgenerator.com)</generator>
        
### 2. Quines etiquetes són obligatoris i quins són opcionals?

Obligatori: title, link, description 
Opcional: language, pubDate, generator 

### 3. Copia la informació d'un <item> i comprova quines etiquetes ha inclòs.
<item>
			<guid isPermaLink="false">2741392115</guid>
			<title>Eight teams on the rinks for the first round of the WSE Champions League Women - World Skate Europe</title>
			<link>https://europe.worldskate.org/eight-teams-on-the-rinks-for-the-first-round-of-the-wse-champions-league/</link>
			<dc:creator>Administrator</dc:creator>
			<category>Rink Hockey</category>
			<description>The first matchday of the WSE Champions League Women, the second of the four phases scheduled for this season, will begin in the next few hours. The first</description>
			<pubDate>Fri, 24 Nov 2023 11:24:11 +0200</pubDate>
			<enclosure url="https://europe.worldskate.org/wp-content/uploads/IMG_4328.jpg" type="image/jpeg" length="0" />
        </item>

### 4. Quines etiquetes són obligatòries i quines opcionals?

    Obligatori: title, link, descrption 
    opcional:   (la resta)

### 5. Compte quants <items> hi ha en total en el fitxer RSS.
    10 items en total

### 6. Fixa't amb el format de les dates, quin fa servir?
    <pubDate>Fri, 24 Nov 2023 11:24:11 +0200</pubDate>

    Dia semana, dia mes any Hora:minuts:segons Zona_horaria


## Activitat 2. Sindicació 

### 1. https://feedly.com, Mitjançant l'eina online, afegirem RSS feeds per poder llegir les notícies que es volem sindicar.Crea 3 carpetes (categories) de diferent tipus, per exemple: 

### Afegeix URLs a cada carpeta per poder seguir les notícies dels blogs.
###    • cine       --> series adicto
###    • sports     --> marca
###    • videogames --> vandal, 3d games


### 2. https://feedreader.com/

### Fes el mateix amb aquesta eina online.


### Activitat 3. Creació d'un fitxer RSS feed 

### Has de crear un fitxer RSS feed d'un blog d'un tema que t'agradi: de notícies esportives, sobre programació, sobre intel·ligència artificial, etc.

###    • El fitxer ha de contenir al menys 3 notícies.
###    • Afegeix un espai de noms per Atom.
###    • L'etiqueta <channel> ha de contenir les següents etiquetes:

<title>
<link>
<description>
<language>
<pubDate>
<lastBuildDate>
<docs>
<category>

### I també l'etiqueta de l'espai de noms atom:
    <atom:link ... />
### Aquesta ha de contenir els atributs href, rel i type

### Consulta el web següent per saber quina informació has de posar en aquests atributs:
### https://www.w3.org/2005/Atom

###    • Els items han de tenir el següent:

<title>
<link>
<description>
<pubDate>
<guid>
<category>




<?xml version="1.0" encoding="ISO-8859-1" ?> 
<rss version="2.0"> 
    <channel>
        <title>rink Hockey</title>
        <link>https://europe.worldskate.org/ </link>
        <description>Home or european skating sports</description>
        <language>en</language>
        <pubDate></pubDate>
        <lastBuildDate>Mon, 27 Nov 2023 09:11:42 +0000</lastBuildDate>
        <docs></docs>
        <category><![CDATA[News]]></category>
        <atom:link href="https://europe.worldskate.org/feed/" rel="self" type="application/rss+xml" />

        <item>  
            <title>WSE Cup Qualifying Round is ready to start with three challenging days  </title>
            <link>https://europe.worldskate.org/wse-cup-qualifying-round-is-ready-to-start-with-three-challenging-days/</link>
            <description>WSE CUP</description>
            <pubDate>Mon, 27 Nov 2023 09:11:42 +0000</pubDate>
            <guid />
            <category><![CDATA[News]]></category>
        </item>

        <item>  
            <title>Official schedule of the WSE Champions League Men Group Phase</title>
            <link>https://europe.worldskate.org/official-schedule-of-the-wse-champions-league-men-group-phase/</link>
            <description>WSE Champions League</description>
            <pubDate>Mon, 27 Nov 2023 09:11:42 +0000</pubDate>
            <guid />
            <category><![CDATA[News]]></category>
        </item>
        
        <item>  
            <title>Olot (Spain) will host the WSE Euro Women from 4 to 9 December 2023</title>
            <link>https://europe.worldskate.org/olot-spain-will-host-the-wse-euro-women-from-4-to-9-december-2023/</link>
            <description>WSE Euro</description>
            <pubDate>Mon, 27 Nov 2023 09:11:42 +0000</pubDate>
            <guid />
            <category><<![CDATA[News]]>/category>
        </item>
    </channel>
</rss>

## Activitat 4 

<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <atom:link href="https://www.technologystore.com/rss.xml" rel="self" type="application/rss+xml" />
        <link>http://www.technologystore.com</link>
        <description>Web store with technology products</description>
        <language>en</language>
        <pubDate>23 Nov 2023 10:25:47 GMT</pubDate>
        <generator>MySitemapGenerator (www.mysitemapgenerator.com)</generator>
        <item>
            <guid isPermaLink="false">3735335495</guid>
            <title>Heavy Lift Delivery Drone with High Payload</title>
            <link>https://www.technologystore.com/blog/heavy-light-delivery-drone-with-high-playload/</link>
            <description>Heavy Lift Delivery Drone with High Payload is a high standard product</description>
            <dc:creator>Administrator</dc:creator>
            <dc:rights>Copyright 2002 Technolody Inc.</dc:rights>
            <atom:author>
                <name>John Doe</name>
                <email>johndoe@example.com</email>
            </atom:author>
            <category>Drones</category>
            <pubDate>23 Nov 2022 12:25:46</pubDate>
            <enclosure url="https://www.technologystore.com//wp-content/uploads/2014/06/logo-103-3.png" />
        </item>
        <item>
            <guid isPermaLink="false">2165437042-AB-33</guid>
            <title>Alpha Mini Programmable and Artificial Intelligent Humanoid Robot</title>
            <link>https://www.technologystore.com/blog/alpha-mini-programable-and-artificial-intelligent-humanoid-robot/</link>
            <dc:creator>Administrator</dc:creator>
            <dc:rights>Copyright 2002 Technolody Inc.</dc:rights>
            <atom:author>
                <name>John Doe</name>
                <email>johndoe@example.com</email>
            </atom:author>
            <category>Robots</category>
            <pubDate>23 Nov 2022 12:25:45</pubDate>
            <enclosure url="https://www.technologystore.com/wp-content/uploads/2014/06/logo-104-3.png" />
        </item>
        <item>
            <guid isPermaLink="false">2661109956-NF-24</guid>
            <title>HAfox GeForce RTX 3090 Graphics Card with 24GB GDDR6X</title>
            <link>https://www.technologystore.com/blog/hafox-geforce-rtx-3090-graphics-card-24gb-gddr6x</link>
            <description>Power Graphics: HAfox GeForce RTX 3090 Graphics Card with 24GB GDDR6X</description>
            <dc:creator>Administrator</dc:creator>
            <dc:rights>Copyright 2002 Technolody Inc.</dc:rights>
           
            <category>Computers</category>
            <pubDate>23 Nov 2022 12:25:44</pubDate>
            <enclosure url="https://www.technologystore.com/wp-content/uploads/2014/06/logo-105-3.png" />
        </item>
        <item>
            <guid isPermaLink="false">53382867-AM-31</guid>
            <title>AMD RYZEN 5 3600 Wof 3.60GHZ Hexa</title>
            <link>https://www.technologystore.com/blog/amd-ryzen-5-3600-wof-360ghz-hexa</link>
            <dc:creator>Administrator</dc:creator>
            <dc:rights>Copyright 2002 Technolody Inc.</dc:rights>
           
            <atom:author>
                <name>John Doe</name>
                <email>johndoe@example.com</email>
            </atom:author>
            <category>Computers</category>
            <pubDate>23 Nov 2022 12:25:43</pubDate>
            <enclosure url="https://www.technologystore.com/wp-content/uploads/2014/06/logo-106-3.png"  />
        </item>
        <item>
            <guid isPermaLink="false">279333011-GR-56</guid>
            <title>AGAO S80 Solar Scooter</title>
            <description><div>
                    <h1>The best solar scooter is AGAO S80 Solar Scooter</h1>
                     <br><p>Photos: <a href="https://www.technologystore.com/wp-content/uploads/2014/06/logo-106-3.png" target="_blank" rel="noopener">Technology Store</a></p>
                </div>
            </description>
            <link>https://www.technologystore.com/blog/agap-s80-solar-scooter</link>
            <dc:creator>Administrator</dc:creator>
            <dc:rights>Copyright 2002 Technolody Inc.</dc:rights>
           
            <atom:author>
                <name>John Doe</name>
                <email>johndoe@example.com</email>
            </atom:author>
            <category>Eco Green</category>
            <pubDate>23 Nov 2022 12:25:42</pubDate>
            <enclosure url="https://www.technologystore.com/wp-content/uploads/2014/06/logo-107-3.png" />
        </item>
    </channel>
</rss>
